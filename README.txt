TatraPay is operated by Tatra Banka, a. s., Slovakia
Payment gateway for Ubercart
www.tatrabanka.sk	

Developed by Marek Zapach & Peter Pokrivcak (halfpixelstudio.com)
Inspired by www.webnthings.co.za

Installation:
No special instructions to install. Upload and enable module as normal.

Configure:
In admin/store/settings/payment enable TatraPay as a payment method. 
Under admin/store/settings/payment/edit/methods, click on TatraPay settings.
Enter the TatraPay MID and Checksome key you were given by TatraPay
Leave Test TatraPay MID and Test Checksum key as is.
Select transaction mode to be Test or Production (defaults to Test). Use test to check your installation. Testing instructions below.
Change return URL to suit your needs. The default is enough to show transaction messages and new user register, etc.
Change titles and instructions to suit your layout.